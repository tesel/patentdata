#!/bin/bash

if ! type gsed > /dev/null; then
  alias gsed="sed";
fi

rare=https://rarediseases.info.nih.gov/diseases/fda-orphan-drugs/
wget --recursive -l 2 --accept-regex '(drugs/[A-Z]|(0-9))|(diseases/[0-9]+)$' $rare
rm -r rarediseases.info.nih.gov/*/**/index.html

for f in ./rarediseases.info.nih.gov/diseases/medical-products-for-selected-diseases/*; do
  ref=$( echo $f | gsed 's/.*\/\([0-9]\+\)/\1/' )
  cat "$f" |hxselect span |gsed "s/<span class=\"info-block-title\">/<tr><td>/; s| (Brand name: \(.*\))|</td><td>\1|; s|</span>|</td><td>$ref</td></tr>|;"
done |hxclean > salida.html

for f in ./rarediseases.info.nih.gov/diseases/fda-orphan-drugs/*; do
  cat $f |hxnormalize |hxclean |hxselect .MedicalProductsDiseaseLinks \
         |gsed 's|<li class="">|<td>|; s|</li>|</td>|' \
         |hxselect td \
         |gsed 's|<td>|<tr><td>|; s|<div class="DeviceMedicalProducts" id="MedicalProducts_\([0-9]*\)" style="display:none;"></div>|</td><td>\1</td></tr>|'
done |hxclean > salida2.html
